async function getCharacters() {

    // declaration d'une variable qui stock le lien dans response
    const response = await fetch("https://jsonplaceholder.typicode.com/users");

    // await response.json(); sert à extraire les données JSON de la réponse HTTP obtenue après l'appel à l'API.
    const characterList = await response.json();
    // pour chaque personnages du tableau
    for (const oneCharacter of characterList) {
        
        // on affiche le nom,la rue, le code postal, la ville
        console.log(`Nom : ${oneCharacter.name}`, `Adresse: ${oneCharacter.address.street}`, oneCharacter.address.zipcode, oneCharacter.address.city);
    }
}
// on appelle la fonction getCharacters
getCharacters();
